My responsibilities included:

1. Design of client-facing app.
2. Writing most of the HTML, CSS, and Jaavascript code for the frontend of the application, including code related to communication with the server (Ajax and REST).
3. Defining and editing certain routes in the backend.
4. Writing of the README.
