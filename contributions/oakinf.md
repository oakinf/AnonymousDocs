My role on this team included researching competitive concurrent editing platforms and their vulnerabilities to ensure our platform correctly eliminates these problems. I put together most of the written parts of our project, as well as the presentation powerpoint. 

I transferred our github repository into gitlab, and created and assigned issues for us to complete. I researched and put together the following markdown files: a majority of Report.md, Requirements.md, and Workflow.md. 
