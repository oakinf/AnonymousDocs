## AnonymousDocs

### Overview
AnonymousDocs is an innovative online platform designed to empower users to collaborate on creating and sharing documents in a private and anonymous manner. The platform boasts a sleek front-end interface developed using HTML, CSS, and Javascript, while the back-end runs on Node.js and MongoDB.

The central feature of AnonymousDocs is its user-friendly interface for document creation, editing, and sharing. Users can quickly create new documents, easily edit existing ones, and invite collaborators to contribute to the document. The platform also offers a unique feature that allows users to create document sections with exclusive edit access, allowing them to maintain complete control over their work. Users can also transfer ownership of the document to collaborators as needed.

AnonymousDocs enables real-time collaboration among users, allowing multiple users to work on the same document simultaneously, thereby making the platform ideal for group projects or brainstorming sessions. Collaborators can also add comments on specific sections of the document, enabling a more streamlined feedback process.

The platform emphasizes user privacy and anonymity by requiring only a username and password for sign-up. This makes AnonymousDocs an ideal platform for users who value their privacy and are looking for a collaborative platform that respects their anonymity.

![Editor Screenshot](resources/Screen_Shot_2023-04-09_at_10.23.10_PM.png)

### Running AnonymousDocs Locally
Running AnonymousDocs is incredibly simple. Simply open a terminal, clone this repository, navigate to the root directory, and install the necessary dependencies using npm:

```
AnonymousDocs% npm install express jsonwebtoken body-parser bcryptjs mongoose dotenv
```

Finally, run the launch.sh script:

```
AnonymousDocs% ./launch.sh  
```

This will set the necessary environment variables (these are set to the default values and can be changed) and start up the server.

At this point, you can open up any browser and go to ```127.0.0.1:8080``` (if you set a different port value, replace 8080 with it) to run the app client.

### Orienting yourself in this repository
The two directories of interest are the root directory and the ```client``` directory. 

1. **Root directory:** The root directory contains all back-end-related code. The ```app.js``` file is where the server is started up, as well as where all routes are defined. ```document.js```, ```section.js```, and ```student.js``` define schemas for database objects.

2. **```client``` directory:** The ```client``` directory is the directory that holds all client-related code. It has four subdirectories: ```css```, ```html```, ```images```, and ```js```. These are all fairly self-explanatory - ```css``` contains all the styles that are applied on the application webpages, ```html``` has all the skeleton HTML code for every page, ```images``` contains any images that are used throughout the application pages, and finally, ``js`` contains all javascript code that runs on the front-end.





