Our team respected the Workflow Requirements in the following ways:

* Used the website application Whereby to coordinate weekly meetings where we would discuss our progress, what we need to get done, issues in the code, our report, and presentation requirements. These online meetings got more frequent as deadlines (presentation, demo) approached.

* Met in person several times to practice our presentation and to ensure the demonstration of our project works correctly.

* Created a groupchat in the Messenger application where we very frequently discussed our project - from bug fixes to commits. This groupchat also helped us divide tasks and update each other immediately an issue was dealt with.

* The use of both github and gitlab to push commits, pull from, and document our code.

* Creating issues in our repository to highlight what was missing from our main branch, mainly for the written components of our project.
