# Report

## Overview of System

AnonymousDocs is a privacy-focused tool for collaborative editing that enables multiple users to edit markdown files in real-time without collecting personally identifiable information. The system supports users in creating, editing, and sharing markdown files, all while maintaining the anonymity of the users. The primary goal of a system is to provide a secure and efficient platform for collaborative work on markdown documents. The system is built using modern web technologies, including MongoDB for cloud storage, and employs a token based authentification system to protect user privacy.

The stakeholders of the system include a diverse range of users who require a collaborative editing platform that ensures their anonymity and privacy. AnonymousDocs is especially useful for individuals and groups who need to work on sensitive documents without revealing their identity - such as whistleblowers, researchers, activists, or journalists. 

The system operators, us, are also recognized as stakeholders, as we are responsible for maintaining and developing the platform, and also have an interest in ensuring the security and privacy of the users. We provide support to users, are concerned with the functionality and usability of the platform, and ensure compliance with privacy regulations and standards.

There are also third party service providers included as stakeholders in our product. AnonymousDocs uses MongoDB to store data securely - and thus MongoDB must also be a stakeholder, as they are responsible for the data they handle. 

Our system was developed in response to growing concerns around information processing, collection, and dissemination in online collaborative tools, which often prioritize convenience over user privacy. Platforms like Google Drive[^1] and Microsoft online brazenly collect an enormous amount of user data, and admit to being able to read through and delete a user's files.[^2] 

AnonymousDocs emphasizes user control and data security. We recognize that many users are hesitant to share PII online due to fears of identity theft, data breaches, and unwanted surveillance. To address these concerns, we have designed AnonymousDocs with privacy by design principles in mind. Our system is built from the ground up to minimize data collection, ensure data accuracy, and provide users with clear control over their data. For example, we do not collect any PII from our users and we do not store any user data on our servers beyond what is necessary for collaborative editing. Additionally, we employ hashing and salting to protect user passwords, and we use TLS encryption[^3] to secure data transmission between our MongoDB server and our backend.

Our system aligns with fair information practices, which emphasize transparency, choice, and user control in data processing. We believe that users should have the right to know how their data is being collected, processed, and used, and they should be able to make informed choices about what data to share and with whom. AnonymousDocs puts the power in the hands of the user by giving them full control over their data, including the ability to delete it permanently at any time. 

In summary, AnonymousDocs is a concurrent editing platform that prioritizes user control and data security. Our system aligns with privacy by design principles and fair information practices, and is designed to meet the needs of users who value privacy and data security in their collaborative work.

## Design and Implementation

### Architecture
It was important to the project team to provide a straightforward startup for new users: Anonymous Documents is a simple application which permits simple instructions. However, we have also left room for the savvier end users to create their own mongodb atlas database account and cluster, and to deploy the backend on a local machine, allowing them maximal control over their data at rest and on the wire. Indeed, we have also imposed no restrictions on accessing the core functionality of the API, so any keen end user is also welcome to implement their own frontend, with or without the need to spin up their own server. The architecture decisions we made were in service of these goals: a simple vanilla  frontend to limit browser compatibility issues, open source ExpressJS backend to enable native transfer of objects to the cloud database, and leveraging environment variables to define the location of the database, the secret code to sign tokens, and the number of salt rounds for password security. These measures enable rapid, modular startup for end users who wish to host their own server, create their own database cluster, or don't know what a terminal is.
 

### Design Decisions
As a collaborative editing application, we defined a collaboration protocol that allows for the coherence and correctness of any document. There exist multiple sections in a document, and only 1 user can edit a section. There are multiple comments to a section. Therefore, a user owns a set of sections and a set of comments. We chose this 'relinquish and lock' decision mainly to ensure that one user can not take over the editing of another - but also because concurrent editing in its true form - where multiple users can delete, change, and edit the work of others in real time - is much more technically challenging, and could not be completed in the scope of time that we had.

To address the course topic in "Minimizing Data Retention" and "Respecting Data Integrity", we designed a policy of deleting of a document and user. When a collaborator quits document, we delete all the sections and comments of the user. To avoid impacting the integrity and coherence of the document, a section owner can transfer the ownership to another collaborator or creator before quitting. When an editor quits a document, the first collaborator is promoted as the editor, and all the comments and sections are deleted. When a user deletes their account, they automatically quit all documents to which they are still associated. This use case is outlined in the delete account sequence diagram. 

### Choice of Technology
As this is a prototype development, we used JavaScript and Express.js for our backend. This stack enables rapid prototyping all the while avoiding major security risks. In addition to this, MongoDB was also used as our database - to securely store the file information for our users on the cloud. For our front end, we also used Vanilla JavaScript, CSS, and HTML to design the template of our website and make the interface user friendly. 

### Implementation Decisions

When a user signs up, we validate the strength of the proposed password, ensuring that it meets certain criteria such as being at least 8 characters long and containing at least one upper case, one lower case letter, one number, and one special character. The password is then securely stored in MongoDB using encryption, hashing, and salting techniques. Without salting, it is possible for an attacker to use a "Rainbow Table" to reverse lookup the password by comparing it to precomputed hash tables. Salting adds an additional layer of security by introducing randomization into the hashing process. While it is still technically possible to reverse lookup the password given a salt and a hash, the space of possible salts is a set of 16-byte integers, making it extremely difficult to store the hash value for all possible salts and perform that lookup.

When a user logs in, we generate a token with encrypted identification information and an expiration time, which is then sent back to the user. This approach ensures that the user's password is not transmitted for each call to the backend, making the application more secure. The token is stored in-memory by the front-end, allowing the user to perform authorized actions within the application without needing to re-enter their login credentials each time.

To ensure data security, we use TLS encrpytion to protect data transmitted from the MongoDB server to our backend. When we deploy the backend using HTTPS, the data transferred in the request body is also encrypted. Despite being a NoSQL database, MongoDB is still vulnerable to Query Injection Attacks. As a result, we took measures during development to avoid queries involving JS functions as input. By relying on the MongoDB driver to handle parameter checks, we can prevent hackers from injecting code to either halt or modify the backend.

### Limitations

Due to time constraints and some team members being ill, we were unable to implement input validation using Regular Expressions to prevent Query Injection Attacks in our course project. As a result, we rely solely on MongoDB to validate the correctness and integrity of user input - we are also avoiding queries that could execute code or change their content based on user input format.

Additionally, it is important to note that the encryption between the front and back end is dependent on the deployment. Encryption is only enabled by protocol when the back end is deployed on an HTTPS server.

## Discussion

The primary purpose of the application is to provide a platform for users to share documents without revealing their identity, or data, to outsiders. The application's main strength is its anonymity feature, which allows users to maintain their privacy while sharing documents. The application succesfully allows for concurrent editing, however, differently than its main competitors - seeing as it allows for users to own sections (all users can not edit all parts of the document). This implementation can admittedly be frustrating at times in terms of usability, but is a reflection of the user-centric approach to privacy (them owning **their own** data, and having the write to do what they want with it) our team has tried to encapture.

Compared to other alternatives, AnonymousDocs is unique in that it does not require users to reveal their identity  or personal information. In addition to this, our organization does not reserve the right to peruse through a user's files - something very different to competing platforms. Traditional cloud storage services like Dropbox or Google Drive prioritize convencience over privacy. While these storage services might offer a better ease of use, they do not provide the same level of privacy and security as AnonymousDocs. Traditional services store user data on their servers, often in plain text, and can be subject to hacking, data breaches, or government surveillance.

One potential downside of AnonymousDocs is that it may require some sacrifice of convenience for the sake of privacy. For example, the lack of email connected to an account means that if a user forgets their password, they have completely lost access to their account. In the future, allowing our clients to opt-in to adding PII (for example: an email or phone number) for easier account recovery might improve the usability of our platform. Another example has to do with the strict data retention design AnonymousDocs implements. In real life, this policy requires clear communication with the user of implications of quitting a document and deleting their account - all aspects of account recovery are made void due to our privacy sensitive design.

In the future, to completely eliminate all potential PII - we might think of randomly generating usernames for each of our clients, instead of allowing them to choose their own. At the moment, allowing clients to choose their names gives these usernames the potential to become PII. We could completely eliminate this potential by assigning names to each user in our database.

Throughout working on this project, we have learned several lessons. Firstly, the trade off between convenience and privacy. If a platform is so privacy secure that it becomes impossible to use, most users would probably take the risk and choose the less secure option. Thankfully, we believe we found a negotiable balance between the two in our implementation. AnonymousDocs is very similar to other concurrent editing platforms, but still remains privacy conscious - proving that these other, large companies do not need to acquire that much information from users to function. 

Adding onto the trade off between functionality and privacy, we also learned that a reliance on third party services is often very much needed. There is a necessity to trusting third party service providers in development and delivery of the app. Re-implementing everything ourselves, especially an entire database, might have made our platform more secure, but it would also have been completely impossible given the time frame we had to work on this project.

Several of our team members were unfamiliar with some languages and technologies used. JavaScript and MongoDB were introduced to a few of us. These technologies definitely offered efficiency and good functionality, but it was difficult as they presented challenges such as a steep learning curve, technical bugs or incompatibilities, and limited documentation.

In our opinion, AnonymousDocs represents a positive step forward in the current technology ecosystem because it demonstrates that privacy-preserving solutions are feasible and necessary. It also shows the possibility to create user-friendly and effective platforms that prioritize privacy, and that this can be achieved with minimal sacrifices to usability and convenience. 


## References
[^1]: https://support.google.com/drive/answer/2450387?hl=en
[^2]:https://security.stackexchange.com/questions/129376/can-microsoft-access-all-private-data-if-a-user-installs-windows-10#:~:text=Data%20that%20leaves%20your%20hard,illegal%20activities%20without%20your%20consent.
[^3]: https://www.mongodb.com/basics/mongodb-encryption

