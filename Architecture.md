### Stakeholders
- Direct Users
  - This is the most basic class of end user. These users will simply navigate to our webpage and start creating. These are the only end users that provide data in the form of comments, text editing, invitations to collaborate, username, and password. This data which is collected to render our services is referred to as user data. 
- Secondary Users
  - These are the end users that implement a frontend which implements our open source api.
- Project Team (We)
  - This consists of the creators of Anonymous Documents (the app) and the associated documentation. We wear different hats throughout the creation and maintenance of the app including project manager, developer, and database administrator.

### Design Decisions
Broadly, the app consists of a frontend and a backend. The frontend is built with vanilla javascript, html, and css. This has minimal negative impact on privacy as compared to using a third party framework, which could have security vulnerabilities, host malicious code, or thereby increase the risk of disclosure of private user data. Another advantage of using plain JS is it removes the need for privacy conscious end users, and the project team itself, to read about the privacy practices of any third party code provider, thereby eliminating one of the downfalls of the privacy by policy strategy. 

The backend is the more technologically complicated product. It consists of a RESTful API and a cloud database used to create, read, update, delete, and persist user data. The API is built on the ExpressJS application framework for NodeJS. We selected this framework because it is broadly trusted in the software engineering community, and it has incredibly low overhead, enabling the project team to offer this service with very little lead time. The database is hosted on the cloud by the MongoDB Atlas platform, which offers a couple built-in privacy enhancing features for the unpaid version: the database is password protected, can only be accessed by an approved IP address, and can only be accessed by project team members with appropriate permissions. The paid version offers much more, including encryption at rest, client side encryption (that is, encrypted before it reaches their servers, not before it reaches our backend), backup scheduling, and activity audits. These extensive features, though behind a paywall, would allow the project team to scale its security features as the app matures. 

Passwords are hashed using the BCrypt library, which uses a slow hashing algorithm, reducing the number of strings an attacker can hash in a given timeframe. What's more, it default adds some random data to the plain text password before hashing, a process known as salting, to nearly eliminate the risk of an attacker using existing plaintext to cyphertext tables to reverse engineer the plaintext password.  This way, a user's password is never stored in plaintext in the database. 

To manage sessions and authentication, we implemented a token based solution using the JWT framework. This is an important consideration for end users who use the app on public computers. We also sign these tokens with a secret code which is not hosted on the public repository: if the token can not be decoded with our secret code, then it did not come from us, and the request is rejected. What's more, since we are not using browser cookies to control user authentication, there is no risk for a cross-site request forgery attack: every request to create,  read, update, or delete user data is accompanied by a JWT in the header.

The backend is not currently hosted on an https server, so all communication from frontend to backend is transferred via http. The same goes for communications to and from the backend and the database. This is a significant security risk which should be handled before deploying to a production environment. In the meantime, users should be careful to create unique usernames and passwords before signing up for the services, in case an eavesdropper might recover your entire login information for another web service used.


### Architectural Models

Please see [../architecture/dbClassDiagramAnonDocs.png]() to view a class diagram describing the database object model and relevant modules used by the application.

Please see [../architecture/RestTree.png]() to view a diagram of the Anonymous Documents backend API.

### Important Scenarios

There are several sequence diagrams detailing the signup, transfer section, delete user, quit document, and invite collaborator scenarios in the architecture directory of the project. 
