# Requirements

## System Purpose and Scope

AnonymousDocs is a new, privacy-sensitive tool that focuses on collaborative editing without the collection of personally identifiable information that most concurrent editing platforms use. Users of this new platform can edit markdown files with ease and invite their collaborators to edit alongside them - all the while their PII remains completely hidden from both other collaborators and us, the creators.

Our website supports collaborative editing in real time, meaning multiple users are able to work on one markdown document. The owner of a file uses just one piece of information, a username, to invite their peers to the document. Each user on a file gets one section that they can lock and relenquish control of. On the right side of the screen, a preview of what the markdown document would look like is displayed to all - updating and reformatting continuously so that the edits of every online user appears immediately. Once a user chooses to delete a file permanently, both them and their data is gone - however one is able to transfer ownership of a file, or a section of the file to another user.

In addition to this, our platform supports the storage of multiple markdown files to the cloud using MongoDB, meaning one does not have to immediately download their .md file after each session. If they do want to, however, they are able to download their markdown file at any point and store it locally on their machine.

Some limits in functionality include the fact that our online editor only works for markdown files, and that users, unlike other popular platforms, are not able to view/edit/share with just a link - they must have an account registered for the service.

## Sample Systems

There are several famous concurrent editing platforms already on the market. However, many of them fail to respect the privacy of their clients.

For one, all other popular collaborative platforms require upon registration some form of PII - most likely a user's name and email. 

Google Drive collects that plus some unecessary, additional pieces of information: a user's phone number, IP address, device information, browser version and type, location information, as well as cookies and usage data. Microsoft Online, another competiting platform, collects close to the same. Both applications vehemently violate the 'minimal collection of personal information' privacy by design principal. 

In comparison, AnonymousDocs collects no cookies (except for ones that keep you logged in), no device or browser information, and no PII (except, potentially, a username).

Unlike our editor, websites like Google Drive and Microsoft Online reserve the right to examine, edit, and delete their clients files - in addition to sharing it with third parties. This means a user is not safe to assume that the file is solely for them and their collaborators. Microsoft  states "Microsoft is legally allowed to examine that information to protect itself, comply with law enforcement, or prevent illegal activities without your [consent."](https://security.stackexchange.com/questions/129376/can-microsoft-access-all-private-data-if-a-user-installs-windows-10#:~:text=Data%20that%20leaves%20your%20hard,illegal%20activities%20without%20your%20consent.) Clearly, for many of these systems, privacy is not the default setting - and user's do not have complete control over their own data. 

Finally, platforms like Google Drive and Dropbox Paper allow for link sharing access to files. This is potentially harmful to the privacy-minded, as many fake scams, based on share links, are reported yearly. A link a user might think is safe might actually be malicious. Our platform eliminates this risk completely by requiring a registered user's username to share a file. 

## Functional Requirements

1. File Management
* Users can create new markdown files
* Users can delete their markdown files
* Users can store their markdown files on the cloud with this application, for easy accessibility and management
* Users can preview markdown files in real time

2. File Download and Storage
* Users can download their markdown files at any time
* Users can store their markdown file in the cloud using MongoDB
* Users can access their files from any device with internet access

3. Collaborative Editing
* Users can invite collaborators to edit a markdown file
* Collaborators can edit the file simultaneously and in real time
* Each collaborator can lock and unlock sections - the owner of a file can assign sections
* Changes made are immediately visible to all users

4. User Interface
* Website is optimized for desktop and mobile use - capable of being used on any device with a browser
* Website has intuitive and easy to use interface
* Interface allows for easy access to file management and editing features
* Interface allows for the preview of markdown files in real time.

## Privacy Requirements

1. Minimal Collection of PII
* No cookies collected (except for session length tracking)
* Only information collected is username - not officially considered PII

2. Database Security
* Database can only be accessed from an approved IP address
* Database is password protected (MongoDB personnel are not able to access it either)
* All MongoDB atlas traffic is TLS encrpted - data is encrypted at rest
* Backups can be scheduled at any interval
* Password salting is used to enhance security of stored passwords

3. Sessions and Authentification
* JavaScript Web Tokens architecture authenticates requests and defines session lengths
* When user logs on, we issue a signed token which contains their username and priviledge level
* Token is used to validate requests to access and edit documents in the AnonymousDocs database

4. Minimal Data Retention
* Users have complete control over their data and files, can can delete files permanently
* Once a user quits a document, all comments made by the user is deleted
* When an account is deleted, the user is removed from all documents
* When a creator/owner of a document quits, one of the collaborators is promoted to the owner - when everyone quits a document it is immediately removed from our database.

## Privacy by Design

Our system is **proactive, not reactive**. The use of a secure database, password salting, and the minimal collection of PII all at registration means that our system is, by default, privacy oriented. We do not collect an exhorbitant amount of information, and thus we do not need to protect and hide that information. One can say the same thing about the way we handle cookies - by barely having them. We by default start with the ability for our users to make themselves anonymous and thus do not need to implement a way to hide these identities from other users and from our organization. For this reason, we do not need to require users to 'opt-out' of any personal information collection - as they have from the get-go. For the same reasons, our website also respects the **privacy as the default setting principle**. 

Similarly, one can say **privacy is embedded to the system** as we start by using password salting, and an encrypted database - in addition to, as already mentioned, the minimal data collected.

**Full functionality** is not lost as a result of being privacy minded. Our system supports concurrent editing fully - multiple users can access, read, write to, and download the same document at the same time, making this website a great way to collaborate effectively with a team - much like rival, less privacy focused, platforms.

**End-to-end security** is seen in the way we protect all the data we are entrusted with. Usernames, passwords, and file information is stored securely within an encrypted database - safe from us, the organization, third parties (MongoDB themselves), and those who may want to hack into the servers. As previously mentioned, passwords are made even safer with the use of salting. 

Our platform is **transparent** about its data collection and retention methods. We lay out our privacy minded viewpoint on our website for our clients to peruse. Nothing is hidden from the users, and we **respect our users' privacy** by allowing them to delete their account at any time without retaining any information about them, not allowing third parties to access their data, and not accessing their data ourselves.
