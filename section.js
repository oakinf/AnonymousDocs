const Mongoose = require("mongoose")

const SectionSchema = new Mongoose.Schema({
    title: String, 
    assignedUser: { type: Mongoose.Schema.Types.ObjectId, ref: 'Student' }, 
    text: String,
    comments: [{ 
        comment: String, 
        date: Date, 
        user: { type: Mongoose.Schema.Types.ObjectId, ref: 'Student' },
        username: String
    }]
}, { collection: "sections" })

exports.Section = Mongoose.model("Section", SectionSchema)