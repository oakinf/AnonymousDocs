const COLLAB_SETTINGS = 
    `<div id="modal-content-add-collab" class="modal-content-subsection">
        <div id="modal-content-subtitle-add" class="modal-content-subtitle">Add Collaborators</div>
        <div id="modal-content-subsection-left">
            <input id="collab-name" type="text" placeholder="Enter Username" required>
            <button id="invite-btn">Invite</button>
        </div>
    </div>
    <div id="modal-content-existing-collabs" class="modal-content-subsection">
        <div id="modal-content-subtitle-existing" class="modal-content-subtitle">Existing Collaborators</div>
        <div id="modal-content-subsection-right"></div>
    </div>`;

const EDIT_DOCUMENT_SECTION = 
    ``;

const ADD_DOCUMENT_SECTION = 
    `<div id="modal-content-add-section">
        <form  id="add-section-form" class="form-container">
            <label for="sectionName">Section Name</label>
            <input id="section-name" type="text" placeholder="Enter Section Name" name="sectionName" required>
        </form>
        <button id="create-section-btn">Create New Section</button>
    </div>`;

const COMMENT_MODAL =
    `<div id="modal-content-comment-section">
    </div>
    <div id="text-and-send">
        <textarea class = 'comment-text-area inline-block-child' id="comment-text-field"></textarea>
        <button class = 'inline-block-child' id="send-comment-button">Send</button>
    </div>
    `;
