const IMAGES_PATH = '../images';


/**
 * On window load, focus text area, set event listeners, 
 * and retrieve textarea text from localStorage.
 */
window.addEventListener('load', function() {
    // setEventListeners();
    getDocumentSections(false);
    // setInterval(function() {
    //     getDocumentSections(true);
    // }, 2000);    
})


function addDocumentSections() {

}

function getCollaborators(token, docid) {
    const getcollabsrequest = new XMLHttpRequest();
    const url = `/document/${docid}/collaborators`;
    getcollabsrequest.open("GET", url, true);
    getcollabsrequest.setRequestHeader("Authorization", `Bearer ${token}`);
    getcollabsrequest.addEventListener('load', function() {
        let html = "";
        console.log(JSON.stringify(this.response))
        const res = JSON.parse(this.response)
        res.collaborators.forEach((collaborator) => {
            html +=
            `<div class="document-collaborator">${collaborator.username}</div>`;
        })
        document.getElementById("modal-content-subsection-right").innerHTML = html
    })
    getcollabsrequest.send()
}

/**
 * Open modal with specified view
 * 
 * @param {String} view  The view to be displayed in the modal.
 *      options are:    'collab-settings', 'edit-document-section', 
 *                      'add-document-section', 'open-comments',
 *                      'transfer-section'           
 * 
 * @param {Object} options View options object. Options depend on the view.
 */
function openModal(view, options) {
    switch (view) {
        case 'collab-settings':
            document.getElementById('modal-title').innerHTML = 'Collaborator Settings';
            document.getElementById('modal-content').innerHTML = COLLAB_SETTINGS;
            break;
        
        case 'edit-document-section':
            //const documentSectionID = options.documentSectionID;
            document.getElementById('modal-title').innerHTML = 'Document Section Settings';
            document.getElementById('modal-content').innerHTML = EDIT_DOCUMENT_SECTION;
            console.log(options.sectionID);
            break;

        case 'add-document-section':
            document.getElementById('modal-title').innerHTML = 'Add Document Section';
            document.getElementById('modal-content').innerHTML = ADD_DOCUMENT_SECTION;

            document.getElementById('create-section-btn').addEventListener('click', function() {
                createSection(document.getElementById('section-name').value);
            });
	    break;

        case 'open-comments':
            document.getElementById('modal-title').innerHTML = 'Comments';
            document.getElementById('modal-content').innerHTML = COMMENT_MODAL;
            console.log(JSON.stringify(options))
            break;

        case 'transfer-section':
            document.getElementById('modal-title').innerHTML = 'Transfer Section Ownership';
            document.getElementById('modal-content').innerHTML = options.html;

            const collabs = Array.from(document.getElementsByClassName('document-collaborator transfer'));
            collabs.forEach(collab => {
                collab.addEventListener('click', () => {
                    const username = collab.getAttribute('username');
                    const collabid = collab.getAttribute('user-id');
                    transferSectionOwnership(options.sectionid, username, collabid);
                });
            })
            break;
    }

    document.getElementById('modal').style.display = 'inline';
    //document.getElementById('editor-page-main-content').style.filter = 'blur(2px)';
}


/**
 * Download the contents of the textarea as a markdown file
 * 
 * @param {String} text The text inside the textarea
 */
function download(text){
    const element = document.createElement('a');
  
    // A blob is a data type that can store binary data
    // "type" is a MIME type
    // It can have a different value, based on a file you want to save
    const blob = new Blob([text], { type: 'text/markdown' });
    const fileUrl = URL.createObjectURL(blob);
    const filename = document.getElementById('filename').innerHTML;
    
    // setAttribute() Sets the value of an attribute on the specified element.
    element.setAttribute('href', fileUrl); // file location
    element.setAttribute('download', filename); // file name
    element.style.display = 'none';
    
    //use appendChild() method to move an element from one element to another
    document.body.appendChild(element);
    element.click();
}


function setSectionEventListeners() {
    // text area listeners
    const textAreas = Array.from(document.querySelectorAll('.md-textarea'));
    textAreas.forEach((textArea) => {
        textArea.addEventListener('input', function(e) {
            console.log('input');
            sendDocumentSectionUpdate(textArea.getAttribute('sectionID'), textArea.value);
            updatePreview();
        });
    });


    // edit document section btn listeners
    const editDocumentSectionBtns = Array.from(document.querySelectorAll('.edit-document-section-btn'));
    editDocumentSectionBtns.forEach((btn) => {
        btn.addEventListener('mouseover', function() {
            btn.src = `${IMAGES_PATH}/edit-document-section-colour.svg`;
        });
        btn.addEventListener('mouseout', function() {
            btn.src = `${IMAGES_PATH}/edit-document-section-bw.svg`;
        });
        btn.addEventListener('click', function() {
            openModal('edit-document-section', {
                sectionID: btn.parentElement.parentElement.children[1].getAttribute('sectionid')
            });
            console.log('here');
            console.log(btn);
        });
    });

    // comment-on-document section btn listeners
    const documentSectionCommentsBtns = Array.from(document.querySelectorAll('.comment-document-section-btn'));
    documentSectionCommentsBtns.forEach((btn) => {
        btn.addEventListener('mouseover', function() {
            btn.src = `${IMAGES_PATH}/chat-colour.svg`;
        });
        btn.addEventListener('mouseout', function() {
            btn.src = `${IMAGES_PATH}/chat-bw.svg`;
        });
    });

    const transferDocumentBtns = Array.from(document.querySelectorAll('.transfer-document-section-btn'));
    transferDocumentBtns.forEach((btn) => {
        btn.addEventListener('mouseover', function() {
            btn.src = `${IMAGES_PATH}/transfer-section-colour.svg`;
        });
        btn.addEventListener('mouseout', function() {
            btn.src = `${IMAGES_PATH}/transfer-section-bw.svg`;
        });
    });

    const deleteSectionBtns = Array.from(document.querySelectorAll('.delete-section-btn'));
    deleteSectionBtns.forEach((btn) => {
        btn.addEventListener('mouseover', function() {
            btn.src = `${IMAGES_PATH}/delete-section-colour.svg`;
        });
        btn.addEventListener('mouseout', function() {
            btn.src = `${IMAGES_PATH}/delete-section-bw.svg`;
        });
    });
}


function transferSectionOwnership(sectionid, username, collabid) {
    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;
    const docID = document.getElementById('filename').getAttribute('doc-id');


    // return if target collab is user itself
    if (username === tokenData.user) {
        document.getElementById('modal').style.display = 'none';
        return;
    }

    const transferOwnershipReq = new XMLHttpRequest();
    const url = `/document/${docID}/section/${sectionid}/transfer`;
    transferOwnershipReq.open("POST", url, true);
    transferOwnershipReq.setRequestHeader("Content-Type", "application/json");
    transferOwnershipReq.setRequestHeader("Authorization", `Bearer ${token}`);

    transferOwnershipReq.addEventListener('load', function() {
        if (this.status == 200) {
            document.getElementById('modal').style.display = 'none';

            const sectionHeading = document.querySelector(`.document-section-heading[sectionid="${sectionid}"]`);
            const sectionTextArea = document.querySelector(`textarea[sectionid="${sectionid}"]`);

            sectionHeading.setAttribute('assigned-username', username);
            sectionHeading.setAttribute('assigned-id', collabid);

            sectionTextArea.setAttribute('assigned-username', username);
            sectionTextArea.setAttribute('assigned-username', username);

            sectionTextArea.classList.remove('owned');
            sectionTextArea.classList.add('not-owned');
        }        
    })    

    const json = {
        id: docID,
        collaborator: username,
        sectionid: sectionid
    }
    console.log(json);
    transferOwnershipReq.send(JSON.stringify(json));

}

/**
 * Set event listeners for transfer-doc button
 */
function setTransferEventListeners() {
    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;
    const username = tokenData.user;
    const userid = tokenData.user_id;
    const docID = document.getElementById('filename').getAttribute('doc-id');

    // show comment thread btn listeners
    const transferButtons = Array.from(document.getElementsByClassName("transfer-document-section-btn"));

    transferButtons.forEach(transferBtn => {
        transferBtn.addEventListener('click', function() {         

            const getcollabsrequest = new XMLHttpRequest();
            const url = `/document/${docID}/collaborators`;
            getcollabsrequest.open("GET", url, true);
            getcollabsrequest.setRequestHeader("Authorization", `Bearer ${token}`);
            getcollabsrequest.addEventListener('load', function() {

                let html = `<div class="transfer-section-subtitle">Select a collaborator to transfer ownership of this section to:</div>`
                html += `<div class="doc-collabs-container">`;
                const res = JSON.parse(this.response)
                console.log(res);
                res.collaborators.forEach((collaborator) => {
                    if (collaborator.username !== username) {
                        html +=
                        `<div 
                            class="document-collaborator transfer"
                            username="${collaborator.username}"
                            user-id="${collaborator.id}">${collaborator.username}</div>`;
                    }
                })
                html += `</div>`;

                const sectionid = transferBtn.parentElement.parentElement.getAttribute('sectionid');
                console.log(transferBtn);
                console.log(sectionid);
                const options = {
                    userid: userid,
                    docid: docID,
                    html: html,
                    sectionid: sectionid
                }
                openModal('transfer-section', options);
            })
            
            getcollabsrequest.send()
        })
    })

    
}

/**
 * Set event listeners for transfer-doc button
 */
function setDeleteSectionListeners() {
    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;
    const username = tokenData.user;
    const userid = tokenData.user_id;
    const docID = document.getElementById('filename').getAttribute('doc-id');
    

    // show comment thread btn listeners
    const deleteButtons = Array.from(document.getElementsByClassName("delete-section-btn"));

    deleteButtons.forEach(delBtn => {
        delBtn.addEventListener('click', function() {
            const secHeader = delBtn.parentElement.parentElement;
            const secId = secHeader.getAttribute("sectionid");
            const textzone = document.querySelector(`textarea[sectionid="${secId}"]`)
            const deleteSectionRequest = new XMLHttpRequest();
            const url = `/document/${docID}/section/${secId}`;
            deleteSectionRequest.open("DELETE", url, true);
            deleteSectionRequest.setRequestHeader("Authorization", `Bearer ${token}`);
            deleteSectionRequest.addEventListener('load', function() {
                if (this.status == 200) {
                    secHeader.remove();
                    textzone.remove();
                }
                else alert('Server error. Please try again later');
            })
            deleteSectionRequest.send()
        })
    })
}

/**
 * Sets up getters and senders for the chat feature.
 * 
 * @param {string} sectionid 
 * @param {string} token 
 */
function setChatEventListeners(sectionid, token) {
    // show comment thread btn listeners
    const commentButton = document.getElementById("chat-btn-" + sectionid)
    const secid = {
        "id" : sectionid
    }
    commentButton.addEventListener('click', function() {
        openModal('open-comments', secid)
        getComments(sectionid, token)
        const sendButton = document.getElementById("send-comment-button")

        sendButton.addEventListener('click', function(){

            const sendcommentrequest = new XMLHttpRequest()
            const docid = document.getElementById("filename").getAttribute("doc-id")
            const commentsurl = `document/${docid}/section/${sectionid}/comment`
            sendcommentrequest.open("POST", commentsurl, true)
            sendcommentrequest.setRequestHeader("Content-Type", "application/json");
            sendcommentrequest.setRequestHeader("Authorization", `Bearer ${token}`);
            const commentText = document.getElementById("comment-text-field").value;
            const json = {
                "comment":commentText
            }
            console.log(json);
            const data = JSON.stringify(json);

            sendcommentrequest.addEventListener('load', function() {
                console.log(JSON.stringify(this.response))
                if (this.status == 200) {
                    getComments(sectionid, token);
                    document.getElementById("comment-text-field").value = "";
                }
            });

            sendcommentrequest.send(data);

        })
    })
}

/**
 * Set all the necessary event listeners on the page
 */
function setInitialEventListeners() {
    // section event listeners
    setSectionEventListeners();

    // close modal listeners
    const closeModalBtn = document.getElementById('close-modal-btn')
    closeModalBtn.addEventListener('click', function() {
        document.getElementById('modal').style.display = 'none';
    });
    document.body.addEventListener('keydown', function(e) {
        if (e.key === 'Escape') {
            document.getElementById('modal').style.display = 'none';
        }
    });

    // download, share, and add section btns liteners
    const navbarBtnNames = ['download', 'collab-settings', 'add-document-section', 'quit-document']
    navbarBtnNames.forEach((btnName) => {
        const btn = document.getElementById(`${btnName}-btn`)
        btn.addEventListener('mouseover', function() {
            btn.src = `${IMAGES_PATH}/${btnName}-colour.svg`;
        });
        btn.addEventListener('mouseout', function() {
            btn.src = `${IMAGES_PATH}/${btnName}-bw.svg`;
        });

        const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
        const token = tokenData.token;
        const docid = document.getElementById("filename").getAttribute("doc-id")
        switch (btnName) {
            case 'collab-settings':
                btn.addEventListener('click', function() {
                    openModal('collab-settings');
                    getCollaborators(token, docid)
                    const invitebtn = document.getElementById("invite-btn")
                    invitebtn.addEventListener('click', function() {
                        const collabname = document.getElementById("collab-name").value
                        const json = {
                            "collaborator":collabname
                        }
                        const data = JSON.stringify(json);
                        const inviterequest = new XMLHttpRequest();
                        const url = `/document/${docid}/collaborators`;
                        inviterequest.open("PUT", url, true);
                        inviterequest.setRequestHeader("Content-Type", "application/json");
                        inviterequest.setRequestHeader("Authorization", `Bearer ${token}`);
                        inviterequest.addEventListener('load', function() {
                            console.log(this.response)
                            getCollaborators(token, docid)
                        })
                        inviterequest.send(data)
                    })
                });
                break;
            case 'quit-document':
                btn.addEventListener('click', function() {
                    const leavedocrequest = new XMLHttpRequest()
                    const url = `/document/${docid}/collaborators`
                    leavedocrequest.open("DELETE", url, true)
                    leavedocrequest.setRequestHeader("Authorization", `Bearer ${token}`)
                    leavedocrequest.addEventListener('load', function() {
                        console.log(this.response)
                    })
                    leavedocrequest.send()
                    window.location.href = window.location.origin + '/dashboard';
                })
                break;
            case 'download':
                btn.addEventListener('click', function() {
                    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
                    const token = tokenData.token;
                    const userID = tokenData.user_id;
                    const username = tokenData.user;
                
                    const request = new XMLHttpRequest();
                    const id = document.getElementById('filename').getAttribute('doc-id');
                    const url = `/document/${id}`;
                    request.open("GET", url, true);
                    request.setRequestHeader("Authorization", `Bearer ${token}`);
                    request.addEventListener("load", function(){  
                        const jsonRes = JSON.parse(this.response);
                        console.log(document)
                        let text = ""
                        for (let section of jsonRes.document.section) {
                            text += section.text + "\n"
                        }
                        download(text);
                    })
                    request.send()
                });
                break;

            case 'add-document-section':
                btn.addEventListener('click', function() {
                    openModal('add-document-section');
                });
                break;
        }
    })

    // unfocus sections not owned by user on click
    const textAreas = Array.from(document.getElementsByClassName('text-editor-content md-textarea not-owned'));
    textAreas.forEach(textArea => {
        textArea.addEventListener('click', unfocusTextArea)
    });
}

function unfocusTextArea() {
    this.blur()
}


/**
 * Update the markdown preview to display latest changes in textarea
 */
function updatePreview() {
    const textAreas = Array.from(document.querySelectorAll('.md-textarea'));
    
    let content = '';
    textAreas.forEach((textArea) => {
        content += `\n\n${textArea.value}`;
    });

    document.getElementById('md-preview').innerHTML = marked.parse(content);
}


//----------------------------------------------//
//       DOCUMENT SECTIONS OPERATIONS           //
//----------------------------------------------//


/**
 * Create a new document section
 * 
 * @param {string} sectionName Name of new section
 */
function createSection(sectionName) {
    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;
    const username = tokenData.user;
    const docID = document.getElementById('filename').getAttribute('doc-id');

    const request = new XMLHttpRequest();
    const id = document.getElementById('filename').getAttribute('doc-id');
    const url = `/document/${docID}/section`;
    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.setRequestHeader("Authorization", `Bearer ${token}`);
    
    request.addEventListener("load", function(){           
        if (this.status == 200) {
            const jsonRes = JSON.parse(this.response);
            const resData = jsonRes.data;
            document.getElementById('modal').style.display = 'none';

            const sectionCount = document.querySelectorAll('.document-section-heading').length;

            const html =
            `<div class="document-section-heading"
                    sectionid="${resData.id}"
                    assigned-id="${resData.assignedUserID}"
                    assigned-username="${resData.assignedUsername}">
                <div class="document-section-title">Section ${sectionCount+1}: ${resData.title}</div>
                <div class="document-section-heading-btns-container">
                    <img id="chat-btn-${resData.id}" class="comment-document-section-btn" src="../images/chat-bw.svg" loading="lazy">
                    <!-- <img class="edit-document-section-btn" src="../images/edit-document-section-bw.svg" loading="lazy"> -->
                    <img class="delete-section-btn visible" src="../images/delete-section-bw.svg" loading="lazy">
                    <img class="transfer-document-section-btn visible" src="../images/transfer-section-bw.svg" loading="lazy">                    
                </div>
            </div>
            <textarea 
                class="text-editor-content md-textarea owned" 
                sectionid="${resData.id}"
                assigned-id="${resData.assignedUserID}"
                assigned-username="${resData.assignedUsername}"></textarea>`;

            document.getElementById('document-sections-container').innerHTML += html;
            setChatEventListeners(resData.id, token)
            setSectionEventListeners();
            setTransferEventListeners();
            setDeleteSectionListeners();
        }
        else alert('Server error. Please try again later');

    }, false);

    const json = { 
        title: sectionName,
    };
    console.log(json);
    const data = JSON.stringify(json);
    request.send(data);    
}

/**
 * Grab the comments for a document section
 *
 * @param {string} sectionid of the comments we want
 */
function getComments(sectionid, token) {
    const getCommentRequest = new XMLHttpRequest();
    const docid = document.getElementById("filename").getAttribute("doc-id")
    const commentsurl = `document/${docid}/section/${sectionid}/comment`
    getCommentRequest.open("GET", commentsurl, true)
    getCommentRequest.setRequestHeader("Authorization", `Bearer ${token}`);
    let commenthtml = "";
    getCommentRequest.addEventListener('load', function() {
        if (this.status == 200) {
            const comments = JSON.parse(this.response)
            comments.comments.forEach((comment) => {
                commenthtml += 
                `<div class='comment'>
                    <div style="margin: 5px 0px;"><b>${comment.username}:</b> ${comment.comment}</div>
                </div>`
            })
        }
        const commentSection = document.getElementById('modal-content-comment-section')
        commentSection.innerHTML = commenthtml;
        commentSection.scrollTop = commentSection.scrollHeight;
    })
    getCommentRequest.send()
}


/**
 * Get textarea text from server
 */
function getDocumentSections() {
    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;
    const username = tokenData.user;
    const userID = tokenData.user_id;

    const request = new XMLHttpRequest();
    const id = document.getElementById('filename').getAttribute('doc-id');
    const url = `/document/${id}`;
    request.open("GET", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.setRequestHeader("Authorization", `Bearer ${token}`);
    
    request.addEventListener("load", function(){           
        if (this.status == 200) {
            const jsonRes = JSON.parse(this.response);
            console.log(jsonRes);
            const sections = jsonRes.document.section;
            let html = ""
            let i = 0;
            sections.forEach(section => {
                let textAreaClass = (section.assignedUser._id === userID) ? "owned" : "not-owned";
                let imageClass = (textAreaClass === 'owned') ? 'visible' : 'hidden';

                html +=
                `<div class="document-section-heading"
                        sectionid="${section._id}"
                        assigned-id="${section.assignedUser._id}"
                        assigned-username="${section.assignedUser.username}">
                    <div class="document-section-title">Section ${i+1}: ${section.title}</div>
                    <div class="document-section-heading-btns-container">
                        <img id="chat-btn-${section._id}" class="comment-document-section-btn" src="../images/chat-bw.svg" loading="lazy">
                        <!-- <img class="edit-document-section-btn" src="../images/edit-document-section-bw.svg" loading="lazy"> -->
                        <img class="delete-section-btn ${imageClass}" src="../images/delete-section-bw.svg" loading="lazy">
                        <img class="transfer-document-section-btn ${imageClass}" src="../images/transfer-section-bw.svg" loading="lazy">                        
                    </div>
                </div>
                <textarea 
                    class="text-editor-content md-textarea ${textAreaClass}" 
                    sectionid="${section._id}"
                    assigned-id="${section.assignedUser._id}"
                    assigned-username="${section.assignedUser.username}">${section.text}</textarea>`;
                i++;
            });
            document.getElementById('document-sections-container').innerHTML = html;
            sections.forEach(section => {
                setChatEventListeners(section._id, token);
            })
            setInitialEventListeners();
            setTransferEventListeners();
            setDeleteSectionListeners();
            updatePreview();
            setInterval(function() {
                getDocumentSectionUpdates();
            }, 300);
        }
        else alert('Server error. Please try again later');

    }, false);

    request.send();
}


/**
 * Update document dections.
 * Exclude sections of this user.
 */
function getDocumentSectionUpdates() {
    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;
    const userID = tokenData.user_id;
    const username = tokenData.user;
    const docID = document.getElementById('filename').getAttribute('doc-id');

    const request = new XMLHttpRequest();
    const id = document.getElementById('filename').getAttribute('doc-id');
    const url = `/document/${id}`;
    request.open("GET", url, true);
    request.setRequestHeader("Authorization", `Bearer ${token}`);
    
    request.addEventListener("load", function(){           
        if (this.status == 200) {
            const jsonRes = JSON.parse(this.response);
            const sections = jsonRes.document.section;  

            let i = 0;
            // update textarea texts
            sections.forEach(section => {
                const assignedID = section.assignedUser._id;

                if (assignedID !== userID) {
                    const sectionID = section._id;
                        const textArea = document.querySelector(`textarea[sectionid="${sectionID}"]`);

                        if (textArea) {
                            textArea.value = section.text;
                            textArea.classList.remove('not-owned');
                            textArea.classList.add('owned');
                        }
                        else {
                            let textAreaClass = (section.assignedUser._id === userID) ? "owned" : "not-owned";
                            let imageClass = (textAreaClass === 'owned') ? 'visible' : 'hidden';

                            const sectionHeading = document.createElement('div');
                            sectionHeading.classList.add('document-section-heading');
                            sectionHeading.setAttribute('sectionid', section._id);
                            sectionHeading.setAttribute('assigned-id', section.assignedUser._id);
                            sectionHeading.setAttribute('assigned-username', section.assignedUser.username);
                            
                            let html =
                            `<div class="document-section-title">Section ${i+1}: ${section.title}</div>
                            <div class="document-section-heading-btns-container">
                                <img id="chat-btn-${section._id}" class="comment-document-section-btn" src="../images/chat-bw.svg" loading="lazy">
                                <!-- <img class="edit-document-section-btn" src="../images/edit-document-section-bw.svg" loading="lazy"> -->
                                <img class="delete-section-btn ${imageClass}" src="../images/delete-section-bw.svg" loading="lazy">
                                <img class="transfer-document-section-btn ${imageClass}" src="../images/transfer-section-bw.svg" loading="lazy">                        
                            </div>`;

                            sectionHeading.innerHTML = html;

                            const sectionTextArea = document.createElement('textarea');
                            sectionTextArea.classList.add('text-editor-content');
                            sectionTextArea.classList.add('md-textarea');
                            sectionTextArea.classList.add('textAreaClass');
                            sectionTextArea.setAttribute('sectionid', section._id);
                            sectionTextArea.setAttribute('assigned-id', section.assignedUser._id);
                            sectionTextArea.setAttribute('assigned-username', section.assignedUser.username);

                            sectionTextArea.innerHTML = section.text;

                            const sectionsContainer = document.getElementById('document-sections-container');
                            sectionsContainer.appendChild(sectionHeading);
                            sectionsContainer.appendChild(sectionTextArea);

                            sectionTextArea.addEventListener('input', function(e) {
                                console.log('input');
                                sendDocumentSectionUpdate(textArea.getAttribute('sectionID'), textArea.value);
                                updatePreview();
                            });

                            const commentBtn = sectionHeading.querySelector(`#chat-btn-${section._id}`);
                            // comment-on-document section btn listeners
                            commentBtn.addEventListener('mouseover', function() {
                                commentBtn.src = `${IMAGES_PATH}/chat-colour.svg`;
                            });
                            commentBtn.addEventListener('mouseout', function() {
                                commentBtn.src = `${IMAGES_PATH}/chat-bw.svg`;
                            });


                            const transferBtn = sectionHeading.querySelector(`.transfer-document-section-btn`);
                            transferBtn.addEventListener('mouseover', function() {
                                transferBtn.src = `${IMAGES_PATH}/transfer-section-colour.svg`;
                            });
                            transferBtn.addEventListener('mouseout', function() {
                                transferBtn.src = `${IMAGES_PATH}/transfer-section-bw.svg`;
                            });
                            transferBtn.addEventListener('click', function() {         

                                const getcollabsrequest = new XMLHttpRequest();
                                const url = `/document/${docID}/collaborators`;
                                getcollabsrequest.open("GET", url, true);
                                getcollabsrequest.setRequestHeader("Authorization", `Bearer ${token}`);
                                getcollabsrequest.addEventListener('load', function() {
                    
                                    let html = `<div class="transfer-section-subtitle">Select a collaborator to transfer ownership of this section to:</div>`
                                    html += `<div class="doc-collabs-container">`;
                                    const res = JSON.parse(this.response)
                                    console.log(res);
                                    res.collaborators.forEach((collaborator) => {
                                        if (collaborator.username !== username) {
                                            html +=
                                            `<div 
                                                class="document-collaborator transfer"
                                                username="${collaborator.username}"
                                                user-id="${collaborator.id}">${collaborator.username}</div>`;
                                        }
                                    })
                                    html += `</div>`;
                    
                                    const sectionid = transferBtn.parentElement.parentElement.getAttribute('sectionid');
                                    console.log(transferBtn);
                                    console.log(sectionid);
                                    const options = {
                                        userid: userid,
                                        docid: docID,
                                        html: html,
                                        sectionid: sectionid
                                    }
                                    openModal('transfer-section', options);
                                })
                                
                                getcollabsrequest.send()
                            });


                            const deleteBtn = sectionHeading.querySelector(`.delete-section-btn`);
                            deleteBtn.addEventListener('mouseover', function() {
                                deleteBtn.src = `${IMAGES_PATH}/delete-section-colour.svg`;
                            });
                            deleteBtn.addEventListener('mouseout', function() {
                                deleteBtn.src = `${IMAGES_PATH}/delete-section-bw.svg`;
                            });
                            deleteBtn.addEventListener('click', function() {
                                const secHeader = deleteBtn.parentElement.parentElement;
                                const secId = secHeader.getAttribute("sectionid");
                                const textzone = document.querySelector(`textarea[sectionid="${secId}"]`)
                                const deleteSectionRequest = new XMLHttpRequest();
                                const url = `/document/${docID}/section/${secId}`;
                                deleteSectionRequest.open("DELETE", url, true);
                                deleteSectionRequest.setRequestHeader("Authorization", `Bearer ${token}`);
                                deleteSectionRequest.addEventListener('load', function() {
                                    if (this.status == 200) {
                                        secHeader.remove();
                                        textzone.remove();
                                    }
                                    else alert('Server error. Please try again later');
                                })
                                deleteSectionRequest.send()
                            })


                            setChatEventListeners(section._id, token);
                        }

                }
                updatePreview();
                i++;
            });            
            updateSectionOwnership(sections, userID, username);
        }
        else alert('Server error. Please try again later');

    }, false);

    request.send();
}


/**
 * Update the ownership of each section
 * 
 * @param {Array} sections
 * @param {string} userID Id of logged-in user
 * @param {string} username user name of logged-in user
 */
function updateSectionOwnership(sections, userID, username) {
    sections.forEach(section => {
        const assignedID = section.assignedUser._id;
        const assignedUser = section.assignedUser.username;
        const sectionID = section._id;

        const sectionHeading = document.querySelector(`.document-section-heading[sectionid="${sectionID}"]`);
        sectionHeading.setAttribute('assigned-id', assignedID);
        sectionHeading.setAttribute('assigned-username', assignedUser);

        const textArea = document.querySelector(`textarea[sectionid="${sectionID}"]`);
        textArea.setAttribute('assigned-id', assignedID);
        textArea.setAttribute('assigned-username', assignedUser);

        if (assignedID === userID) {
            textArea.classList.remove('not-owned');
            textArea.classList.add('owned');
            textArea.removeEventListener('click', unfocusTextArea);

            const transferSectionImg = sectionHeading.querySelector('.document-section-heading-btns-container > .transfer-document-section-btn');
            const deleteSectionImg = sectionHeading.querySelector('.document-section-heading-btns-container > .delete-section-btn');

            transferSectionImg.classList.remove('hidden');
            transferSectionImg.classList.add('visible');  
            deleteSectionImg.classList.remove('hidden');
            deleteSectionImg.classList.add('visible');  
        }
        else {
            textArea.classList.remove('owned');
            textArea.classList.add('not-owned');
            textArea.addEventListener('click', unfocusTextArea);

            const transferSectionImg = sectionHeading.querySelector('.document-section-heading-btns-container > .transfer-document-section-btn')
            const deleteSectionImg = sectionHeading.querySelector('.document-section-heading-btns-container > .delete-section-btn');

            transferSectionImg.classList.remove('visible'); 
            transferSectionImg.classList.add('hidden');
            deleteSectionImg.classList.remove('visible'); 
            deleteSectionImg.classList.add('hidden');
        }

    });
}



/**
 * Send textarea text to server
 */
function sendDocumentSectionUpdate(sectionID, sectionText) {
    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;

    const docID = document.getElementById('filename').getAttribute('doc-id');

    const request = new XMLHttpRequest();
    const url = `/document/${docID}/section/${sectionID}`;
    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.setRequestHeader("Authorization", `Bearer ${token}`);
    
    request.addEventListener("load", function(){           
        if (this.status == 200) {
            console.log(this.response);
        }
        else alert('Server error. Please try again later');

    }, false);

    const json = { 
        sectiontext: sectionText
    };
    console.log(json);
    const data = JSON.stringify(json);
    request.send(data);
}
