window.addEventListener('load', function() {
    const signupbtn = document.getElementById("signup-btn")
    signupbtn.addEventListener('click', function() {
        const user = document.getElementById('username-input').value;
        const pass = document.getElementById('password-input').value;
        const json = {
            "username": user,
            "password": pass
        }

        const request = new XMLHttpRequest();
        const url = '/user/signup';
        request.open("POST", url, true);
        request.setRequestHeader("Content-Type", "application/json");
                
        request.addEventListener("load", function(){           
            if (this.status == 200) {
                const resJson = JSON.parse(this.responseText);
                console.log(resJson);

                if (resJson && resJson.success) {                    
                    let tokenData = {
                            user: user,
                            token: resJson.token,
                            user_id: resJson.user._id
                        }
                    window.localStorage.setItem('anonymous-docs-token-data', JSON.stringify(tokenData));
                    window.location.href = window.location.origin + '/dashboard';
                }
                else if (resJson && !resJson.success) {
                    document.getElementById('response-msg-div').innerHTML = `Error: ${resJson.error}`;
                    document.getElementById('response-msg-div').style.display = 'flex';
                }                
            }
            else alert('Server error. Please try again later');
    
        }, false);
        
        const data = JSON.stringify(json);
        request.send(data);
    })
})