


window.addEventListener('load', function() {

    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;

    const request = new XMLHttpRequest();
    const url = '/document';
    request.open("GET", url, true);
    request.setRequestHeader("Authorization", `Bearer ${token}`);
    
    request.addEventListener("load", function(){           
        if (this.status == 200) {
            const jsonResponse = JSON.parse(this.response);
            if (jsonResponse.success) {            
                const docs = jsonResponse.doc;

                const docsContainer = document.getElementById('uploaded-docs-content');
                let html = "";
                docs.forEach(doc => {
                    html += 
                    `<div class="document-entry" doc-name="${doc.title}" doc-id="${doc._id}">
                        <div class="filename-text">${doc.title}</div>
                    </div>`;
                    
                });
                docsContainer.innerHTML = html;

                setEventListeners();
            }
            else {
                // handle other stuff
            }
            console.log(this.response);
        }
        else alert('Server error. Please try again later');

    }, false);

    request.send();
})


function setEventListeners() {


    const logoutBtn = document.getElementById('logout-btn');
    logoutBtn.addEventListener('click', function() {
        window.localStorage.removeItem('anonymous-docs-token-data');
        window.location.href = window.location.origin;
    });

    const docs = Array.from(document.querySelectorAll('.document-entry'));
    const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
    const token = tokenData.token;

    docs.forEach(doc => {
        doc.addEventListener('dblclick', function() {
            const form = document.getElementById('doc-form');
            document.getElementById('doc-name-input').value = doc.getAttribute('doc-name');
            document.getElementById('doc-id-input').value = doc.getAttribute('doc-id');
            document.getElementById('token-input').value = token;
            form.submit();
        })
    })

    const createDocument = document.getElementById('create');
    createDocument.addEventListener('click', function() {
        const title = document.getElementById('doc-title').value;
        const request = new XMLHttpRequest();
        const url = '/document';
        const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'));
        const token = tokenData.token;
        request.open("POST", url, true);
        request.setRequestHeader("Content-Type", "application/json");
        request.setRequestHeader("Authorization", `Bearer ${token}`);
        const json = {
            "title": title
        }
        const data = JSON.stringify(json);
        request.addEventListener('load', function() {
            window.location.href = window.location.origin + '/dashboard';
        })
        request.send(data);
    })  
    
    const deleteAccountButton = document.getElementById('delete-account-btn')
    deleteAccountButton.addEventListener('click', function() {
        console.log('deleting')
        const request = new XMLHttpRequest()
        const url = '/document'
        const tokenData = JSON.parse(window.localStorage.getItem('anonymous-docs-token-data'))
        const token = tokenData.token
        request.open("DELETE", url, true)
        request.setRequestHeader("Authorization", `Bearer ${token}`)
        request.addEventListener('load', function() {
                console.log(this.status)
                if (this.status == 200) {
                    const deleteAccountRequest = new XMLHttpRequest()
                    const durl = '/user'
                    deleteAccountRequest.open("DELETE", durl, true)
                    deleteAccountRequest.setRequestHeader("Authorization", `Bearer ${token}`)
                    deleteAccountRequest.addEventListener('load', function() {
                        if (this.status == 200) {
                            console.log('Deleted Account')
                            window.location.href = window.location.origin;
                        }
                    })
                    deleteAccountRequest.send()
                }
        })
        request.send()
    })
}

function showForm() {
    document.getElementById("createdoc").style.display = "flex";
}

function closeForm() {
    document.getElementById("createdoc").style.display = "none";
}


