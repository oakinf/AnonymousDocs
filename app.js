const Express = require("express")
const JWToken = require("jsonwebtoken")
const BodyParser = require("body-parser")
const BCrypt = require("bcryptjs")
const Mongoose = require("mongoose")
const Path = require("path")
const ejs = require('ejs');

require("dotenv").config();

const DB_URL = process.env.DB_URL
const SECRET_CODE = process.env.SECRET_CODE
const SERVER_PORT = parseInt(process.env.SERVER_PORT)
const SALT_ROUNDS = parseInt(process.env.SALT_ROUNDS)
const TOKEN_EXP_SECS = parseInt(process.env.TOKEN_EXP_SECS)


// strong password: https://www.section.io/engineering-education/password-strength-checker-javascript/
// The password is at least 8 characters long (?=.{8,}).

// The password has at least one uppercase letter (?=.*[A-Z]).

// The password has at least one lowercase letter (?=.*[a-z]).

// The password has at least one digit (?=.*[0-9]).

// The password has at least one special character ([^A-Za-z0-9]).
let passwordTester = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})/
let usernameTester = /^[A-Za-z][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*$/

let signToken = (student) => {
    return JWToken.sign({ 
        id: student._id.toString()
    }, SECRET_CODE, { // default SHA256
        expiresIn: TOKEN_EXP_SECS 
    })
}

let checkToken = (req) => {
    return new Promise((res, rej) => {
        if (req.headers && req.headers.authorization) {
            let auth = req.headers.authorization.split(' ')[1];
            let decoded;
            try {
                decoded = JWToken.verify(auth, SECRET_CODE)
            } catch (e) {
                rej(e)
                return
            }
            let stid = decoded.id
            StudentDAO.Student.findOne({ _id: stid })
            .select("-password")
            .then((s) => {
                res(s)
            })
            .catch((err) => {
                rej("wrong token")
            })
        } 
        else if (req.query && req.query.token) {
            let auth = req.query.token;
            let decoded;
            try {
                decoded = JWToken.verify(auth, SECRET_CODE)
            } catch (e) {
                rej(e)
                return
            }
            let stid = decoded.id
            StudentDAO.Student.findOne({ _id: stid })
            .select("-password")
            .then((s) => {
                res(s)
            })
            .catch((err) => {
                rej("wrong token")
            })
        }
        else{
            rej("no token submitted")
        }
    })
}

const app = Express()

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set("views", Path.join(__dirname, "/client/html"));
app.use(Express.static(Path.join(__dirname, "/client")));
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));


try {
    Mongoose.connect(DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
} catch (err) {
    console.log(err);
}


const StudentDAO = require("./student")
const DocumentDAO = require("./document")
const SectionDAO = require("./section")


app.listen(SERVER_PORT, () => {
    console.log("server up at " + SERVER_PORT)
})


/**
 * Gets the homepage view. 
 */
app.get("/", (req, res) => {
    res.sendFile(Path.join(__dirname, "/client/html/homepage.html"))
})

/**
 * Gets the login view.
 */
app.get("/login", (req, res) => {
    res.sendFile(Path.join(__dirname, "/client/html/login.html"))
})

/**
 * Gets the signup view.
 */
app.get("/sign-up", (req, res) => {
    res.sendFile(Path.join(__dirname, "/client/html/sign-up.html"))
})

/**
 * Gets the dashboard view.
 */
app.get("/dashboard", (req, res) => {
    res.sendFile(Path.join(__dirname, "/client/html/dashboard.html"))
})

/**
 * Gets the editor view and passes back the document id and name.
 */
app.get("/editor", (req, res) => {
    // console.log(req);
    checkToken(req)
    .then((student) => {
        res.render('editor', { docId: req.query.docId, filename: req.query.docName });
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Registers a new user with the database if the password and username are compliant. 
 */
app.post("/user/signup", (req, res) => {
    if (!req.body.username || !req.body.password) {
        res.json({ success: false, error: "signup info not complete"})
        return
    }
    let password = req.body.password
    if (!passwordTester.test(password)) {
        res.json({ success: false, error: "Password too simple"})
        return
    }
    let username = req.body.username
    if (!usernameTester.test(username)) {
        res.json({ success: false, error: "Username only contains alphanumeric and underscore"})
        return
    }
    let p = BCrypt.hashSync(password, SALT_ROUNDS)
    StudentDAO.Student.create({
        username: username, 
        password: p
    }).then((student) => {
        res.json({ success: true, token: signToken(student), user: student })
    }).catch((err) => {
        res.json({ success: false, error: "Username already taken!" })
    })
})

/**
 * Registers a JWT to the user if the password is correct.
 */
app.post("/user/login", (req, res) => {
    // console.log(req.body);
    if (!req.body.username || !req.body.password) {
        res.json({ success: false, error: "login info not complete"})
        // res.sendFile(Path.join(__dirname, "/client/html/editor.html"))
        return
    }
    StudentDAO.Student.findOne({ username: req.body.username })
    .then((student) => {
        if (!student) {
            // res.sendFile(Path.join(__dirname, "/client/html/login.html"))
            res.json({ success: false, error: "Wrong Credentials"})
            return
        }
        if (!BCrypt.compareSync(req.body.password, student.password)) {
            // res.sendFile(Path.join(__dirname, "/client/html/login.html"))
            res.json({ success: false, error: "Wrong Credentials" })
            return
        }
        delete student.password;
        res.json({ success: true, token: signToken(student), user: student })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Removes user from db.
 */
app.delete("/user", (req, res) => {
    checkToken(req)
    .then((student) => {
        if (student.documents.length > 0) {
            res.json({ success: false, error: "please quit from all documents before deleting" })
        }
        StudentDAO.Student.findByIdAndDelete(student._id).then((studentdel) => {
            delete studentdel.password;
            res.json({ success: true, error: "user delete success", user: studentdel })
        }).catch((err) => {
            res.json({ success: false, error: "user delete failed" })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Creates a new document, returns the document object including id and title.
 */
app.post("/document", (req, res) => {
    console.log(req.body);
    checkToken(req)
    .then((student) => {
        if (!req.body.title) {
            res.json({ success: false, error: "document must have title" })
            return
        }
        DocumentDAO.Document.create({
            title: req.body.title, 
            creator: new Mongoose.Types.ObjectId(student._id)
        }).then((doc) => {
            console.log(student.creator)
            student.documents.push(doc._id)
            doc.creator = student._id
            student.save().then((ssv) => {
                doc.save().then((sdoc) => {
                    res.json({ success: true, document: doc })
                }).catch((err) => {

                })
                
            }).catch((err) => {
                res.json({ success: false, error: err })
            })
        }).catch((err) => {
            res.json({ success: false, error: err })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Utility to assist in cleanup when a user quits a document.
 */
let coUpdateStudentDocument = (res, student, doc, revertDocument) => {
    let prevSections = doc.section

    doc.section.forEach(section => {
        let sectionComments = section.comments.filter(comment => comment.user.toString() !== student._id.toString())
        section.comments = sectionComments
        section.save().then((ssv)=> {
            console.log('saved')
        }).catch((err) => {
            console.log('not saved')
        })
    });
    
    let sections2del = doc.section.filter(sec => sec.assignedUser.toString() === student._id.toString()).map((s) => s._id)
    doc.section = doc.section.filter(sec => sec.assignedUser.toString() !== student._id.toString())    
    doc.save().then((dsv) => {
        student.save().then((ssv) => {
            SectionDAO.Section.deleteMany({ _id: { $in: sections2del } }).then((secs) => {
                if (res) res.json({ success: true, document: dsv, student: ssv, sections: secs })
                else return true
            }).catch((err) => {
                if (res) res.json({ success: false, error: "sections delete failed" })
                else return false
            })
        }).catch((err2) => {
            doc.section = prevSections
            revertDocument(doc)
            doc.save().then((dreverted) => {
                if (res) res.json({ success: false, error: "doc update reverted, user update failed" })
                else return false
            }).catch((err) => {
                if (res) res.json({ success: false, error: "doc update not reverted, user update failed" })
                else return false
            })
        })
    }).catch((err) => {
        if (res) res.json({ success: false, error: "doc update failed" })
        else return false
    })
}

/**
 * Adds a collaborator to a given document.
 * 
 * Returns the updated document.
 */
app.put("/document/:docid/collaborators", (req, res) => {
    checkToken(req)
    .then((student) => {
        if (!req.body.collaborator) {
            res.json({ success: false, error: "must name a collaborator to share with." })
            return
        }
        DocumentDAO.Document.findOne({ 
            $and: [
                {
                    _id: req.params.docid, 
                }, 
                {
                    creator: student._id
                }
            ]
        })
        .then((doc) => {
            if (doc.creator.toString() !== student._id.toString()) {
                res.json({ success: false, error: "unauthd access" })
                return
            }
            StudentDAO.Student.findOne({ username: req.body.collaborator.toString() }).then((collabo) => {
                if (collabo._id.toString() === student._id.toString()) {
                    res.json({ success: false, error: "u wonna become both?" })
                    return
                }
                if (collabo.documents.includes(doc._id.toString())) {
                    res.json({ success: false, error: "double join?" })
                    return
                } else {
                    collabo.documents.push(doc._id)
                    doc.collaborators.push(collabo._id)
                }
                doc.save().then((dsv) => {
                    collabo.save().then((ssv) => {
                        res.json({ success: true, document: dsv })
                    }).catch((err2) => {
                        doc.collaborators.pop()
                        doc.save().then((dreverted) => {
                            res.json({ success: false, error: "doc update reverted, user update failed" })
                        }).catch((err) => {
                            res.json({ success: false, error: "doc update not reverted, user update failed" })
                        })
                    })
                }).catch((err) => {
                    res.json({ success: false, error: "doc update failed" })
                })
            }).catch((err) => {
                res.json({ success: false, error: "cannot find collaborator" })
            })
        })
        .catch((err) => {
            res.json({ success: false, error: err })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Utility to quit document. Removes the document if there are no more collaborators.
 * 
 * @param {*} res 
 * @param {*} student 
 * @param {*} doc 
 * @returns The updated document and updated user information.
 */
let quitADoc = (res, student, doc) => {
    if (!student.documents.includes(doc._id.toString())) {
        if (res) res.json({ success: false, error: "not joined" })
        else return false
    }
    let idx = student.documents.indexOf(doc._id.toString())
    if (idx > -1) {
        student.documents.splice(idx, 1);
    }
    // creator
    console.log(doc)
    if (doc.creator.toString() === student._id.toString()) {
        if (doc.collaborators.length > 0) {
            // promote the first collaborator as the next creator
            let nextCreator = doc.collaborators.shift()
            doc.creator = nextCreator
            return coUpdateStudentDocument(res, student, doc, (docr) => {
                docr.collaborators.unshift(nextCreator)
                docr.creator = student._id
            })
        } else {
            // remove doc
            student.save().then((ssv) => {
                let sections2del = doc.section.filter(sec => sec.assignedUser.toString() === student._id.toString()).map((s) => s._id)
                DocumentDAO.Document.findByIdAndDelete(doc._id).then((docdel) => {
                    SectionDAO.Section.deleteMany({ _id: { $in: sections2del } }).then((secs) => {
                        if (res) res.json({ success: true, document: docdel, user: ssv })
                        else return false
                    }).catch((err) => {
                        if (res) res.json({ success: false, error: "sections delete failed, doc not recovered" })
                        else return false
                    })
                }).catch((err) => {
                    student.documents.push(doc._id)
                    student.save().then((ssrv) => {
                        if (res) res.json({ success: false, error: "doc delete failed, user revert success" })
                        else return false
                    }).catch((err3) => {
                        if (res) res.json({ success: false, error: "doc delete failed, user revert failed" })
                        else return false
                    })
                })
            }).catch((err) => {
                if (res)
                res.json({ success: false, error: "user save failed" })
                else return false
            })
        }
    } else {
        // collaborator, just remove
        let idx = student.documents.indexOf(doc._id.toString())
        if (idx > -1) {
            student.documents.splice(idx, 1);
        }
        idx = doc.collaborators.indexOf(student._id.toString())
        if (idx < 0) {
            if (res)
            res.json({ success: false, error: "document db link inconsistent error"})
            else return false
        } else {
            doc.collaborators.splice(idx, 1)
        }
        return coUpdateStudentDocument(res, student, doc, (docr) => {
            docr.collaborators.push(student._id)
        })
    }
}

/**
 * Removes the token bearer from the document.
 */
app.delete("/document/:docid/collaborators", (req, res) => {
    checkToken(req)
    .then((student) => {
        DocumentDAO.Document.findOne({ _id: req.params.docid })
        .populate('section')
        .then((doc) => {
            return quitADoc(res, student, doc)
        })
        .catch((err) => {
            res.json({ success: false, error: err })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Removes the token bearer from the document.
 */
app.delete("/document", (req, res) => {
    checkToken(req)
    .then((student) => {
        DocumentDAO.Document.find({ 
            $or: [
                {
                    collaborators: student._id
                }, 
                {
                    creator: student._id
                }
            ] 
        })
        .populate('section')
        .then((docs) => {
            for (let doc of docs) {
                if (!quitADoc(undefined, student, doc)) {
                    res.json({ success: false, error: "failed to del some doc" })
                    return
                }
            }
            res.json({ success: true, error: "all successfully deleted" })
        })
        .catch((err) => {
            res.json({ success: false, error: err })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Gets the document with the supplied parameter id.
 * 
 * Returns the document or fail if wrong permissions or document doesn't exist.
 */
app.get("/document/:docid", (req, res) => {
    checkToken(req)
    .then((student) => {
        if (student.documents.includes(req.params.docid.toString())) {
            DocumentDAO.Document.findOne({ 
                $and: [
                    {
                        _id: req.params.docid.toString(), 
                    }, 
                    {
                        $or: [
                            {
                                collaborators: student._id
                            }, 
                            {
                                creator: student._id
                            }
                        ]
                    }
                ]
            }).populate({
                path : 'section',
                populate : {
                    path : 'assignedUser', 
                    select: '-password -documents'
                }
            }).then((doc) => {
                res.json({ success: true, document: doc })
            }).catch((err) => {
                res.json({ success: false, error: err })
            })
        } else {
            res.json({ success: false, error: "unauth access 2 doc" })
        }
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

app.get("/document", (req, res) => {
    checkToken(req)
    .then((student) => {
        DocumentDAO.Document.find({ 
            $and: [
                {
                    _id: { $in: student.documents }, 
                }, 
                {
                    $or: [
                        {
                            collaborators: student._id
                        }, 
                        {
                            creator: student._id
                        }
                    ]
                }
            ]
        }).populate({
            path : 'section',
            populate : {
                path : 'assignedUser', 
                select: '-password -documents'
            }
        }).then((docsFound) => {
            console.log(docsFound)
            res.json({ success: true, doc: docsFound, user: student })
        }).catch((err) => {
            res.json({ success: false, error: "2" })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: "3", message: err })
    })
})

/**
 * Route to fetch the comments associated with a document section.
 */
app.get("/document/:docid/section/:sectionid/comment", (req, res) => {
    checkToken(req)
    .then((student) => {
        DocumentDAO.Document.findOne({ 
            $and: [
                {
                    _id: req.params.docid, 
                }, 
                {
                    $or: [
                        {
                            collaborators: student._id
                        }, 
                        {
                            creator: student._id
                        }
                    ]
                }
            ]
        })
        .then((doc) => {
            if (student.documents.includes(doc._id.toString())) {
                SectionDAO.Section.findOne({ _id: req.params.sectionid })
                .then((section) => {
                    res.json({success : true, comments: section.comments})
                })
                .catch((err) => {
                    console.log('didnt find the section')
                    res.json({success : false, error: err})
                })
            }
    })
    .catch((err) => {
        res.json({success : false, error: err})
    })
    })
})

/**
 * Route to fetch the usernames of the collaborators on a given document.
 */
app.get("/document/:docid/collaborators", (req, res) => {
    checkToken(req)
    .then((student) => {
        DocumentDAO.Document.findOne({_id: req.params.docid})
        .populate({
            path : 'collaborators creator',
            select: '-password -documents'
        })
        .then((document) => {
            let listeDesCollabo = []
            for (let nom of document.collaborators) {
                listeDesCollabo.push({
                    username: nom.username,
                    id: nom._id
                });
            }
            listeDesCollabo.push({
                username: document.creator.username,
                id: document.creator._id
            });
            res.json({success: true, collaborators: listeDesCollabo})
        })
        .catch((err) => {
            res.json({success:false, error: err})
        })
    })
    .catch((err) => {
        res.json({success:false, error: err})
    })
})

/**
 * Post a comment to a section. Ensures that the commenter has adequate rights.
 */
app.post("/document/:docid/section/:sectionid/comment", (req, res) => {
    checkToken(req)
    .then((student) => {
        if (!req.body.comment) {
            res.json({ success: false, error: "must have comment data" })
            return
        }
        DocumentDAO.Document.findOne({ 
            $and: [
                {
                    _id: req.params.docid, 
                }, 
                {
                    $or: [
                        {
                            collaborators: student._id
                        }, 
                        {
                            creator: student._id
                        }
                    ]
                }
            ]
        })
        .then((doc) => {
            if (student.documents.includes(doc._id.toString())) {
                SectionDAO.Section.findById(req.params.sectionid)
                .then((sec) => {
                    sec.comments.push({
                        user: student._id, 
                        comment: req.body.comment, 
                        date: Date.now(),
                        username: student.username
                    })
                    sec.save().then((secsv) => {
                        res.json({ success: true, error: secsv })
                    }).catch((errd) => {
                        res.json({ success: false, error: "cannot add new comment" })
                    })
                })
                .catch((err) => {
                    res.json({ success: false, error: err })
                })
            } else {
                res.json({ success: false, error: "unauth access 2 doc" })
            }
        })
        .catch((err) => {
            res.json({ success: false, error: err })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Creates a new section on the given document.
 * 
 * Returns the details of the created session.
 */
app.post("/document/:docid/section", (req, res) => {
    checkToken(req)
    .then((student) => {
        if (!req.body.title) {
            res.json({ success: false, error: "section must have title." })
            return
        }
        DocumentDAO.Document.findOne({ 
            $and: [
                {
                    _id: req.params.docid, 
                }, 
                {
                    $or: [
                        {
                            collaborators: student._id
                        }, 
                        {
                            creator: student._id
                        }
                    ]
                }
            ]
        })
        .then((doc) => {
            if (student.documents.includes(doc._id.toString())) {
                SectionDAO.Section.create({
                    title: req.body.title, 
                    assignedUser: student._id, 
                    text: ""
                }).then((newsec) => {
                    doc.section.push(newsec._id)
                    doc.save().then((docsv) => {
                        res.json({ success: true, data: {
                                id: newsec._id,
                                title: req.body.title,
                                assignedUserID: student._id,
                                assignedUsername: student.username,
                            }
                        })
                    }).catch((errd) => {
                        SectionDAO.Section.findByIdAndDelete(newsec._id).then((dels) => {
                            res.json({ success: false, error: "cannot add new section 0", errcont: errd })
                        })
                        .catch((eerr) => {
                            res.json({ success: false, error: "cannot add new section 1" })
                        })
                        
                    })
                }).catch((err_CreateSec) => {
                    res.json({ success: false, error: "cannot add new section 2" })
                })
            } else {
                res.json({ success: false, error: "unauth access 2 doc" })
            }
        })
        .catch((err) => {
            res.json({ success: false, error: err })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Transfers the section to the user specified in the body of the request.
 * 
 * Returns the updated document and section that was transferred.
 */
app.post("/document/:docid/section/:sectionid/transfer", (req, res) => {
    checkToken(req)
    .then((student) => {
        if (!req.body.collaborator) {
            res.json({ success: false, error: "must provie collaborator" })
            return
        }
        StudentDAO.Student.findOne({ username: req.body.collaborator }).then((collabo) => {
            if (collabo._id.toString() === student._id.toString()) {
                res.json({ success: false, error: "u wonna transfer2 yourself?" })
                return
            }
            SectionDAO.Section.findOne({ 
                $and: [
                    {
                        _id: req.params.sectionid, 
                    }, 
                    {
                        assignedUser: student._id
                    }
                ]
            })
            .then((foundsec) => {
                if (!foundsec) {
                    res.json({ success: false, error: "unauthd access" })
                    return
                }
                DocumentDAO.Document.findOne({ 
                    $and: [
                        {
                            _id: req.params.docid, 
                        }, 
                        {
                            $or: [
                                {
                                    collaborators: student._id
                                }, 
                                {
                                    creator: student._id
                                }
                            ]
                        }, 
                        {
                            $or: [
                                {
                                    collaborators: collabo._id
                                }, 
                                {
                                    creator: collabo._id
                                }
                            ]
                        }, 
                        {
                            section: foundsec._id
                        }
                    ]
                })
                .then((doc) => {
                    if (!doc) {
                        res.json({ success: false, error: "unauthd access" })
                        return
                    }
                    foundsec.assignedUser = new Mongoose.Types.ObjectId(collabo._id)
                    foundsec.save().then((savedSec) => {
                        res.json({ success: true, doc: doc, section: savedSec })
                    }).catch((err_save_sec) => {
                        res.json({ success: false, error: "cannot save section" })
                    })
                })
                .catch((err) => {
                    res.json({ success: false, error: err })
                })
            })
            .catch((find_sec_err) => {
                res.json({ success: false, error: "no such section" })
            })
        }).catch((err) => {
            res.json({ success: false, error: "cannot find collaborator" })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Transfers the section to the user specified in the body of the request.
 * 
 * Returns the updated document and section that was transferred.
 */
app.delete("/document/:docid/section/:sectionid", (req, res) => {
    checkToken(req)
    .then((student) => {
        SectionDAO.Section.findOne({ 
            $and: [
                {
                    _id: req.params.sectionid, 
                }, 
                {
                    assignedUser: student._id
                }
            ]
        })
        .then((foundsec) => {
            if (!foundsec) {
                res.json({ success: false, error: "unauthd access" })
                return
            }
            DocumentDAO.Document.findOne({ 
                $and: [
                    {
                        _id: req.params.docid, 
                    }, 
                    {
                        $or: [
                            {
                                collaborators: student._id
                            }, 
                            {
                                creator: student._id
                            }
                        ]
                    }, 
                    {
                        section: foundsec._id
                    }
                ]
            })
            .then((doc) => {
                if (!doc) {
                    res.json({ success: false, error: "unauthd access" })
                    return
                }
                SectionDAO.Section.findByIdAndDelete(foundsec._id).then((deldsec) => {
                    let newSections = []
                    for (let sec2keep of doc.section) {
                        if (foundsec._id.toString() === sec2keep._id.toString()) {
                            continue
                        }
                        newSections.push(sec2keep)
                    }
                    doc.section = newSections
                    doc.save().then((saveddocacvecsecdel) => {
                        res.json({ success: true, section: saveddocacvecsecdel })
                    }).catch((err) => {
                        res.json({ success: false, error: "failed to save doc but sec deleted" })
                    })
                }).catch((err_save_sec) => {
                    res.json({ success: false, error: "failed to save section, doc not affected" })
                })
            })
            .catch((err) => {
                res.json({ success: false, error: err })
            })
        })
        .catch((find_sec_err) => {
            res.json({ success: false, error: "no such section" })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

/**
 * Writes the provided text to the provided document and section.
 */
app.post("/document/:docid/section/:sectionid", (req, res) => {
    checkToken(req)
    .then((student) => {
        if (!req.body.sectiontext) {
            res.json({ success: false, error: "must have text" })
            return
        }
        DocumentDAO.Document.findOne({ 
            $and: [
                {
                    _id: req.params.docid, 
                }, 
                {
                    $or: [
                        {
                            collaborators: student._id
                        }, 
                        {
                            creator: student._id
                        }
                    ]
                }
            ]
        })
        .then((doc) => {
            if (student.documents.includes(doc._id) &&
                (doc.collaborators.includes(student._id.toString()) ||
                 doc.creator.toString() === student._id.toString())) {
                SectionDAO.Section.findOne({ _id: req.params.sectionid })
                .then((foundsec) => {
                    if (foundsec.assignedUser.toString() === student._id.toString()) {
                        foundsec.text = req.body.sectiontext
                        foundsec.save().then((savedSec) => {
                            res.json({ success: true, doc: doc, section: savedSec })
                        }).catch((err_save_sec) => {
                            res.json({ success: false, error: "cannot save section" })
                        })
                    } else {
                        res.json({ success: false, error: "unauthroized section" })
                    }
                })
                .catch((find_sec_err) => {
                    res.json({ success: false, error: "no such section" })
                })
            } else {
                res.json({ success: false, error: "unauth access 2 doc" })
            }
        })
        .catch((err) => {
            res.json({ success: false, error: err })
        })
    })
    .catch((err) => {
        res.json({ success: false, error: err })
    })
})

app.listen(SERVER_PORT, "10.121.67.57", () => {
    console.log('up at port ' + SERVER_PORT)
})
